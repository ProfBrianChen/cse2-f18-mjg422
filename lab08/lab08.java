public class lab08{
  public static void main(String args[]){
    
     int[] numArray = new int[100];
     int[] countArray = new int[100];
    
    //set initial values equal to 0 for all
    for(int i = 0; i < numArray.length; i++){
      countArray[i] = 0;
    }
    
    
      for(int i = 0; i < numArray.length; i++){
        int addVal = (int) (Math.random()*99);
        numArray[i] = addVal;
        countArray[addVal]++;
      }

    
    for(int i = 0; i < countArray.length; i++){
      String times = countArray[i]>1 ? ("times") : ("time");
      System.out.println(i+" occurs "+countArray[i]+" "+times);
    
    }
    
  }
}