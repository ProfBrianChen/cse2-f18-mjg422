//Matthew J Gunotn CSE2
//this program randomly picks a card

import java.lang.Math;

public class CardGenerator{
  public static void main(String args[]){
    
    //pick a random number
    int card = (int) (Math.random()*52)+1;
    
    String suitName = "";
    String cardIdentity = "";
    
    //helps us decide what suit the card is
    if(card <= 13){
      suitName = "Diamonds";
    }
    else if(card >= 14 && card <= 26){
      suitName = "Clubs";
    }
    else if(card >= 27 && card<= 39){
      suitName = "Hearts";
    }
    else if(card >= 40 && card<=52){
      suitName = "Spades";
    }
    else{
      System.out.println("Failure to pick card"+card);
    }
    
    //helps us distinguish between number cards and face cards
    switch(card%13){
      case 11:
        cardIdentity = "Jack";
        break;
      case 12:
        cardIdentity = "Queen";
        break;  
      case 0:
        cardIdentity = "King";
        break;  
      case 1:
         cardIdentity = "Ace";
      
        
      default:
        int holder = card%13;
        cardIdentity = ""+holder;
        break;
    }
    
    //prints everything out for the user
    System.out.println("your card number was "+card);
    System.out.println("You picked the "+cardIdentity+" of "+suitName);
    
  }
}