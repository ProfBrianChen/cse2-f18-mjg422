//Matthew J Gunton CSE2 311
//This program, given acres of land affected and average inches of rain dropped, will convert quantity of rain into miles

import java.util.Scanner;

public class Convert{
  
  //we need to fix:
    //issue with our conversions I believe
  
  public static void main(String args[]){
    
    //introduce the scanner
    Scanner myScanner = new Scanner(System.in);
    
    //grab variables from user
    System.out.println("Enter the affected area in acres: ");
    double acresAffected = myScanner.nextDouble();
    
    //convert acres to square inches
    final double acresToSquareInches = 6272640;
    acresAffected = acresAffected*acresToSquareInches;
    
    //assuming the user is entering in standard inches --> not cubic inches
    System.out.println("Enter the rainfall in the affected area: ");
    double averageRainInches = myScanner.nextDouble();
    
    //total rain:
      //squareInches*inches
    double totalRainCubInches = acresAffected*averageRainInches;
    
    //finally convert cubic inches to cubic miles
    final double cubInchesToCubMiles = 3.93147e-15;
    
    double cubicMilesHit = totalRainCubInches*cubInchesToCubMiles;
    
    
    System.out.println(cubicMilesHit+" cubic miles");
    
  }
}