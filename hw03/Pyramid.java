//Matthew J Gunton CSE2 311
//This program will, given the dimensions of a pyramid, return the volume of said pyramid

import java.util.Scanner;

public class Pyramid{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("The square side of the pyramid is:");
    double squareSide = myScanner.nextDouble();
    
    System.out.println("The height of the pyramid is: ");
    double height = myScanner.nextDouble();
    
    double volume = (height/3) * Math.pow(squareSide, 2);
    
    System.out.println("The volume inside the pyramid is: "+volume);
    
  }
}