//Matthew J Gunton CSE02 - 311
//This program, given the total of a check, how many people are in the group and the percentage tip
  //will determine how much each person is due to pay for the meal

import java.util.Scanner;

public class Check{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in);
    
    //prompting user for check cost
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //grabbing value from user:
    double checkCost = myScanner.nextDouble();
    
    //prompting user for percent tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    //grabbing value from user:
    double tipPercent = myScanner.nextDouble();
      //convert tip percent into a decimal, so it's usable
      tipPercent = tipPercent/100;
    
    //prompting user for number of people in group
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //calculations:
    double totalCost;
    double costPerPerson;
      //variables to help account for rounding
      int dollars;
      int dimes;
      int pennies;
    
    totalCost = checkCost*(1+tipPercent);
    costPerPerson = totalCost/numPeople;
    
    //this will not round, and only give us the value of whole dollars
    dollars = (int) costPerPerson;

     //my algorithm where we find the decimals that are remaining and then find the first value of the decimals
    dimes = (int) ((costPerPerson-dollars)*10);
  
    //using modulo, here it moves up the decimal to account only for the decimal values after dimes
      //then it uses the modulo operator to get rid of everything except the remainder, which gives us our pennies value
    pennies= (int) ((costPerPerson * 100) % 10);
    
    
    //output final answer to user:
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + "" + pennies);


  }
}