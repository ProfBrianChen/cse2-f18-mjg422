import java.util.Random;
import java.util.Scanner;

public class storyPhase2{

   public static void main (String args[]){
       Random randomGenerator = new Random();
     
      int randAdj = randomGenerator.nextInt(10);
      String adj = adj(randAdj);
     
      int randSubNoun = randomGenerator.nextInt(10);
      String subNoun = subNoun(randSubNoun);
     
      int randObjNoun = randomGenerator.nextInt(10);
      String objNoun = objNoun(randObjNoun);
     
      int randPastVerb = randomGenerator.nextInt(10);
      String pastVerb = pastVerb(randPastVerb);
     
     System.out.println("The "+adj+" "+subNoun+" "+pastVerb+" "+objNoun+".");
     
     int sentenceNum = randomGenerator.nextInt(10);
     for(int i = 0; i < sentenceNum; i++){
       randPastVerb = randomGenerator.nextInt(10);
       randSubNoun = randomGenerator.nextInt(10);
       randAdj = randomGenerator.nextInt(10);
       randObjNoun = randomGenerator.nextInt(10);
        
       System.out.println("The "+adj(randAdj)+" "+subNoun(randSubNoun)+" "+pastVerb(randPastVerb)+" "+objNoun(randObjNoun)+".");
     }
     
     //now we need to to add on to it:
      int randActVerb = randomGenerator.nextInt(10);
      String actVerb = actVerb(randActVerb);
     
     System.out.println("So the "+adj+" "+subNoun+" "+actVerb+" "+objNoun+".");
     
     
   }
  
  public static String actVerb(int m){
        switch(m){
      case 0:
        return "spit at";
      case 1:
        return "started burying";
      case 2:
        return "yelled at";
      case 3:
        return "talked crap about";
      case 4:
        return "wrote an angry email to";       
      case 5:
        return "made an angry finsta post";
      case 6:
        return "smacked";
      case 7:
        return "cursed out";
      case 8:
        return "forgave";
      case 9:
        return "hugged and then when it let its guard down robbed";   
      default:
          System.out.println("error");
          return "bad";
    }
  }
  
  public static String adj(int m){
    switch(m){
      case 0:
        return "cool";
      case 1:
        return "savage";
      case 2:
        return "funny";
      case 3:
        return "perfidious";
      case 4:
        return "terrible";       
      case 5:
        return "great";
      case 6:
        return "boring";
      case 7:
        return "innovative";
      case 8:
        return "incredible";
      case 9:
        return "amazing";   
      default:
          System.out.println("error");
          return "bad";
    }
  }
  
  public static String subNoun(int m){
        switch(m){
      case 0:
        return "dog";
      case 1:
        return "fox";
      case 2:
        return "man";
      case 3:
        return "woman";
      case 4:
        return "kid";       
      case 5:
        return "king";
      case 6:
        return "queen";
      case 7:
        return "leader";
      case 8:
        return "tyrant";
      case 9:
        return "stupido";   
      default:
          System.out.println("error");
          return "bad";
    }
  }
  
  public static String objNoun(int m){
        switch(m){
      case 0:
        return "the dog";
      case 1:
        return "the fox";
      case 2:
        return "the man";
      case 3:
        return "the woman";
      case 4:
        return "the kid";       
      case 5:
        return "the king";
      case 6:
        return "the queen";
      case 7:
        return "the leader";
      case 8:
        return "the tyrant";
      case 9:
        return "the stupido"; 
      default:
          System.out.println("error");
          return "bad";
    }
  }
  
  public static String pastVerb(int m){
       switch(m){
      case 0:
        return "stole from";
      case 1:
        return "hit";
      case 2:
        return "ran from";
      case 3:
        return "cried because of";
      case 4:
        return "skipped away from";       
      case 5:
        return "hugged";
      case 6:
        return "hated";
      case 7:
        return "loved";
      case 8:
        return "didn't know";
      case 9:
        return "was married to";   
      default:
          System.out.println("error");
          return "bad";
    }
  }
  
}
