//Matthew J Gunton CSE2

import java.util.Scanner;
import java.lang.Math;

public class CrapsSwitch{
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Do you want to randomly cast dice or state two dice values?");
    System.out.println("Type '1' for randomly cast & '2' for state values");

    int choice = myScanner.nextInt();
    
    switch(choice){
      case 1:
      int die1 = (int)(Math.random()*6+1);
      int die2 = (int)(Math.random()*6+1);
        switch( die1 + die2 ){
           case 2:
            System.out.println("You got snake eyes");
            break;
          case 3:
            System.out.println("You got ace deuce");
            break;
          case 4:
            System.out.println("you got hard four");
            break;
          case 5:
            System.out.println("you got fever five");
            break;
          case 6:
            System.out.println("you got easy 6");
            break;
          case 7:
            System.out.println("you got seven out");
            break;
          case 8:
            System.out.println("you got easy 8");
            break;
          case 9:
            System.out.println("you got nine");
            break;
          case 10:
            System.out.println("you got easy 10");
            break;
          case 11:
            System.out.println("you got yo-leven");
            break;
          case 12:
            System.out.println("you got boxcars");
            break;
          default:
            System.out.println("error");
            break;
        }
        break;
        
      case 2:
        System.out.println("Write your first value");
        int value1 = myScanner.nextInt();
        System.out.println("Write your second value");
        int value2 = myScanner.nextInt();
        switch( value1 + value2 ){
           case 2:
            System.out.println("You got snake eyes");
            break;
          case 3:
            System.out.println("You got ace deuce");
            break;
          case 4:
            System.out.println("you got hard four");
            break;
          case 5:
            System.out.println("you got fever five");
            break;
          case 6:
            System.out.println("you got easy 6");
            break;
          case 7:
            System.out.println("you got seven out");
            break;
          case 8:
            System.out.println("you got easy 8");
            break;
          case 9:
            System.out.println("you got nine");
            break;
          case 10:
            System.out.println("you got easy 10");
            break;
          case 11:
            System.out.println("you got yo-leven");
            break;
          case 12:
            System.out.println("you got boxcars");
            break;
          default:
            System.out.println("error");
            break;
        }
        break;
      default:
        System.out.println("You entered an inavlid value\nprogram terminating");
        break;
        
    }
    
  }
  
}