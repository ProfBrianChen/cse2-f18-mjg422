//Matthew J Gunton CSE2|311

import java.lang.Math;
import java.util.Scanner;

public class CrapsIf{
  public static void main(String args[]){
        
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Do you want to randomly cast dice or state two dice values?");
    System.out.println("Type 'random' for randomly cast & 'state' for state values");

    String choice = myScanner.nextLine();
    
    if( choice.equals("random") ){
      int die1 = (int)(Math.random()*6+1);
      int die2 = (int)(Math.random()*6+1);
      
      if( die1 == 1&& die2 == 1 ){
        System.out.println("You got snake eyes");
      }
            //this one counts the double one
      if( die1 == die2 && die1>=2 && die1<=5 ){
        int name = die1*2;
        System.out.println("You got hard "+name);
      }
      if( die1 == 6 && die2 == 6 ){
        System.out.println("You got boxcars");
      }
      if( die1 == 1 && die2 == 2 ){
        System.out.println("You got ace deuce");
      }
      
      //handles easy ___ going across diagram
      if( die1-die2 == 2 || die2-die1 == 2 ){
        int name = die1+die2;
        System.out.println("You got easy "+name);
      }
      
      //handle seven out
      if( die1 + die2 == 7 ){
        System.out.println("You got seven out");
      }
      
      //handle fever five
      if( die1 + die2 == 5 ){
        System.out.println("You got fever five");
      }
      
      if( die1 + die2 == 9 ){
        System.out.println("You got nine");
      }
      
      if( die1 + die2 == 11 ){
        System.out.println("You got yo-leven");
      }
      
      if( die1 + die2 == 6 ){
        System.out.println("You got easy 6");
      }
      
      if( die1 + die2 == 8 ){
        System.out.println("You got easy 8");
      }
      
      if( die1 == 2 && die2 == 1 ){
        System.out.println("You got ace deuce");
      }

      
    }
    else if( choice.equals("state") ){
      System.out.println("Write your first value");
      int die1 = myScanner.nextInt();
      if( die1 > 0 && die1<=6 ){
        System.out.println("Write your second value");
        int die2 = myScanner.nextInt();
        if( die2 > 0 && die2<=6 ){
          //same options from before, very inefficient code, but as of now I don't know to to use a method to avoid rewritng this:
              if( die1 == 1 && die2 == 1 ){
                System.out.println("You got snake eyes");
              }
                //this one counts the double one
              if( die1 == die2 && die1>=2 && die1<=5 ){
                int name = die1*2;
                System.out.println("You got hard "+name);
              }
              if( die1 == 6 && die2 == 6 ){
                System.out.println("You got boxcars");
              }
              if( die1 == 1 && die2 == 2 ){
                System.out.println("You got ace deuce");
              }

              //handles easy ___ going across diagram
              if( die1-die2 == 2 || die2-die1 == 2 ){
                int name = die1+die2;
                System.out.println("You got easy "+name);
              }

              //handle seven out
              if( die1 + die2 == 7 ){
                System.out.println("You got seven out");
              }

              //handle fever five
              if( die1+die2==5 ){
                System.out.println("You got fever five");
              }

              if( die1+die2==9 ){
                System.out.println("You got nine");
              }

              if( die1+die2==11 ){
                System.out.println("You got yo-leven");
              }

              if( die1+die2 == 6 ){
                System.out.println("You got easy 6");
              }

              if( die1+die2 == 8 ){
                System.out.println("You got easy 8");
              }

              if( die1 == 2&& die2 == 1 ){
                System.out.println("You got ace deuce");
              }
        }else{
          System.out.println("you did not enter in a good value\nprogram terminating");
        }
      }else{
        System.out.println("you did not enter in a good value\nprogram terminating");
      }
    }
    else{
      System.out.println("You did not choose either of the acceptable options\nProgram terminating");
    }
    
  }
}