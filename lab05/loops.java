//Matthew J Gunton CSE2-311
//goal is to ask the user for their course, specifically:
//course number
//department name
//number of times it meets
//time the class starts
//instructor name
//number of students

import java.util.Scanner;

public class loops{
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Welcome to the Matthew Gunton Class Entry Program");
    //q1
    System.out.println("\nPlease enter your course number");
    boolean isCourseANum = myScanner.hasNextInt();
    while(isCourseANum  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter an integer");
      isCourseANum = myScanner.hasNextInt();
    }
    int courseNum = myScanner.nextInt();

    
    //q2
    System.out.println("\nPlease enter your department name");
    String holder = myScanner.nextLine();
    boolean isDeptAStr = myScanner.hasNextLine();
    while(isDeptAStr  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter some department name");
      holder = myScanner.nextLine();
      isDeptAStr = myScanner.hasNextLine();
    }
    String courseDept = myScanner.nextLine();
    
    //q3
    System.out.println("\nPlease enter number of times per week you meet");
    boolean isTimesANum = myScanner.hasNextInt();
    while(isTimesANum  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter an integer");
      isTimesANum = myScanner.hasNextInt();
    }
    int courseTimes = myScanner.nextInt();
        
    //q4
    System.out.println("\nPlease enter what time your class meets in military time");
    boolean isTimeANum = myScanner.hasNextInt();
    while(isTimeANum  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter an integer");
      isTimeANum = myScanner.hasNextInt();
    }
    int courseStartTime = myScanner.nextInt();
        
    //q5
    System.out.println("\nPlease enter the name of the instructor");
    String holderB = myScanner.nextLine();
    boolean isNameAStr = myScanner.hasNextLine();
    while(isNameAStr  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter some name");
      holderB = myScanner.nextLine();
      isNameAStr = myScanner.hasNextLine();
    }
    String courseInstructor = myScanner.nextLine();    
        
    //q6
    System.out.println("\nPlease enter the number of students");
    boolean isStuNumInt = myScanner.hasNextInt();
    while(isStuNumInt  == false){
      String removeGarbage = myScanner.nextLine();
      System.out.println("Please enter an integer");
      isStuNumInt = myScanner.hasNextInt();
    }
    int stuNum = myScanner.nextInt();  
    
    System.out.println("\n\nYour course number is: "+courseNum);
    System.out.println("Your department name is: "+courseDept);
    System.out.println("Your class meets "+courseTimes+" times per week");
    System.out.println("Your class starts at "+courseStartTime);
    System.out.println("Your course's instructor is "+courseInstructor);
    if(stuNum > 1){
       System.out.println("Your course have "+stuNum+" students");
    }else{
       System.out.println("Your course has "+stuNum+" student");
    }

    
  }
}