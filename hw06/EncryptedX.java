import java.util.Scanner;

public class EncryptedX {

    public static void main(String args[]){
        Scanner myScanner = new Scanner(System.in);

        System.out.println("X drawer:");

        System.out.println("how big do you want your X to be?");

        //makes sure the user's input is valid
        boolean isNumHandsInt = myScanner.hasNextInt();
        while(isNumHandsInt == false){
            String garbageCollector = myScanner.nextLine();
            System.out.println("enter in hand number in integer form");
            isNumHandsInt = myScanner.hasNextInt();
        }
        int squareSize = myScanner.nextInt();

        for(int m = 0; m < squareSize; m++){
            int spaceA = squareSize - m;
            int spaceB = m;
            for(int i = 1; i <= squareSize; i++){
                if(i == spaceA || i == spaceB){
                    System.out.print(" ");
                }else {
                    System.out.print("*");
                }
            }
            System.out.print("\n");
        }



    }

}
