public class lab09{
  
  
  
  
  public static void main(String args[]){
    
    int[] array0 = {0,1,2,3,4,5,6,7};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    System.out.println("look above, array0");
    inverter2(array1);
    print(array1);
    System.out.println("look above, array1");
    
    int[] array3 = inverter2(array2);
    print(array3);
    System.out.println("look above, array2");
    
    
  }
  
  public static int[] copy (int arr[]){
    int[] nova = new int[arr.length];
    for(int i = 0; i < arr.length;i++){
      nova[i] = arr[i];
    }
    return nova;
  }
  
  public static void inverter (int arr[]){
    int[] nova = new int[arr.length];
    
    for(int i = 0; i < Math.round(arr.length/2); i++){
      nova[i] = arr[arr.length-i-1];
      nova[arr.length-i-1] = i;
    }  
    
    

    for(int i = 0; i < arr.length; i++){
      arr[i] = nova[i];
    }
   
    
  }
  
  public static int[] inverter2 (int arr[]){
    int[] nova = copy(arr);
    
    inverter(nova);
    
    return nova;
    
  }
  
  public static void print (int arr[]){
    for(int i = 0; i < arr.length; i++){
      System.out.println(arr[i]);
    }
  }
  
}