import java.util.Scanner;
public class Shuffling {
 public static void main(String[] args) {
  Scanner myScanner = new Scanner(System.in);
  //suits club, heart, spade or diamond 
  String[] suitNames = {
   "C",
   "H",
   "S",
   "D"
  };
  String[] rankNames = {
   "2",
   "3",
   "4",
   "5",
   "6",
   "7",
   "8",
   "9",
   "10",
   "J",
   "Q",
   "K",
   "A"
  };
   
   
  String[] cards = new String[52];
  String[] hand = new String[5];
   
  int numCards = 0;
  int again = 1;
  int index = 51;
  for (int i = 0; i < 52; i++) {
     cards[i] = rankNames[i % 13] + suitNames[i / 13];
     System.out.print(cards[i] + " ");
  }
   
  System.out.println();
  printArray(cards);
   
  while (again == 1) {
    //this is Chen's code
    //this should be changed so it shuffles every time
    //my addition is here:
    shuffle(cards);
     hand = getHand(cards, index, numCards);
     printArray(hand);
     index = index - numCards;
     System.out.println("Enter a 1 if you want another hand drawn");
     again = myScanner.nextInt();
  }
 }
  
  public static void printArray(String cards[]){
    for(int i = 0; i < cards.length; i++){
      System.out.print(cards[i]+" ");
    }
  }
  
  public static String[] shuffle(String cards[]){
    int shuffleTimes = 100;
    for(int i = 0; i < shuffleTimes; i++){
      int newSpot = (int) (Math.random()*51+1);
      String old = cards[0];
      cards[0] = cards[newSpot];
      cards[newSpot] = old; 
    }
    return cards;
  }
  
  public static String[] getHand(String cards[], int index, int numCards){
    //if statement below is for numCards > cards.length
    if(numCards > cards.length){
      //recreating the deck
        String[] suitNames = {
         "C",
         "H",
         "S",
         "D"
        };
        String[] rankNames = {
         "2",
         "3",
         "4",
         "5",
         "6",
         "7",
         "8",
         "9",
         "10",
         "J",
         "Q",
         "K",
         "A"
        };        
        for (int i = 0; i < 52; i++) {
           cards[i] = rankNames[i % 13] + suitNames[i / 13];
           System.out.print(cards[i] + " ");
        }
    }
    
    if(numCards >= 0){
      numCards = 1;
    }
    
    String[] hand = new String[numCards];
    int counter = 0;
    for(int i = cards.length-index-1; i > cards.length-index-1-numCards; i--){
      hand[counter] = cards[i];
      counter++;
    }
    return hand;
  }
  
  
}