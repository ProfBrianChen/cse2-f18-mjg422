import java.util.Scanner;

public class hw07{
    public static void main (String args[]){

        Scanner myScanner = new Scanner(System.in);
        String sample = sampleText(myScanner);
        System.out.println("You entered: "+sample);

        String commands = "";
        char option;
        do{
            option = printMenu(myScanner);
            commands+=option;
        }
        while(option != 'q');

        for(int i = 0; i < commands.length(); i++){
            char operator = commands.charAt(i);
            if(operator == 'c'){
                int lengthSansWS = getNumOfNonWSCharacters(sample);
                System.out.println("Number of non-whitespace characters "+lengthSansWS);
            }
            if(operator == 'w'){
                int numWords = getNumOfWords(sample);
                System.out.println("Number of words: "+numWords);
            }
            if(operator == 'f'){
                System.out.println("Enter a word or phrase you wanted to find: ");
                String garbage = myScanner.nextLine();
                String find = myScanner.nextLine();
                int matches = findText(find, sample);
                System.out.println(" \""+find+"\" instances: "+matches);
            }
            if(operator == 'r'){
                sample = replaceExclamation(sample);
                System.out.println("sans exclamation: \n"+sample);
            }
            if(operator == 's'){
                sample = shortenSpace(sample);
                System.out.println("No double space: \n"+sample);
            }

        }


    }

    public static String shortenSpace(String text){

        String newOne = "";
        int lastStop = 0;

        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) == ' '){
                int numSpace = 1;
                while(text.charAt(i + numSpace) == ' '){
                    numSpace++;
                }

                for(int m = lastStop; m <= i; m++){
                    newOne += text.charAt(m);
                }
                lastStop = i+numSpace;
            }
            if(i == text.length() - 1){
                for(int m = lastStop; m <= i; m++){
                    newOne += text.charAt(m);
                }
            }
        }

        return newOne;



    }

    public static String replaceExclamation(String text){

        String newOne = "";
        int lastBreak = 0;
        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) == '!'){
                //how do we replace at a certain index
                for(int m = lastBreak; m < i; m++){
                    newOne += text.charAt(m);
                }
                lastBreak = i+1;
                newOne+= '.';
            }
            if(i == text.length() - 1){
                for(int m = lastBreak; m <= i; m++){
                    newOne += text.charAt(m);
                }
            }
        }

        return newOne;

    }

    public static int findText(String find, String text){
        int match = 0;
        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) == find.charAt(0)){
                for(int m = 1; m < find.length();m++){
                    if(text.charAt(i+m) != find.charAt(m)){
                        break;
                    }
                    if(m == find.length()-1){
                        match++;
                    }
                }
            }
        }
        return match;
    }

    public static int getNumOfWords(String text){
        int numWords = 0;
        int letters = 0;
        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) == ' ' && letters != 0 || i == text.length()-1){
                numWords++;
                letters = 0;
            }
            if(text.charAt(i) != ' '){
                letters++;
            }
        }

        return numWords;

    }

    public static int getNumOfNonWSCharacters(String text){

        int lengthSansWS = 0;
        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) != ' '){
                lengthSansWS++;
            }
        }
        return lengthSansWS;

    }

    public static String sampleText(Scanner scanner){
        System.out.println("Enter a sample text:");

        String sample = scanner.nextLine();

        return sample;

    }

    public static char printMenu(Scanner myScanner){


        while(true){

            System.out.print("c - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\n");
            System.out.println("Choose an option:");

            char input = myScanner.next().charAt(0);

            if(input == 'c' || input == 'w' || input == 'f' || input == 'r' || input == 's' || input == 'q'){
                return input;
            }
            System.out.println("INVALID RESPONSE!");
        }

    }

}
