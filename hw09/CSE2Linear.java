//Matthew J Gunton CSE2 Chen

import java.util.Scanner;

public class CSE2Linear {

    public static void main(String args[]){

        int[] scores = new int[10];

        //give scores an initial value for each index
        for(int i = 0; i < scores.length; i++){
            scores[i] = 0;
        }

        int counter = 0;

        while(counter < 10){

            System.out.println("enter in score for test #"+(counter+1));
            int intermediate = compare();
            if(intermediate < 0){
                continue;
            }
            //so long as the scores could be the same, letting this exception remain in code
            if(counter != 0 && intermediate < scores[counter-1]){
                System.out.println("next score must be bigger than "+scores[counter-1]);
                continue;
            }
            scores[counter] = intermediate;


            counter++;
        }

        //print out the array
        for(int i = 0; i < scores.length; i++){
            System.out.println(scores[i]);
        }

        //good practice to make sure the user's goal meets the same criteria as before
        //let's throw this into its own method
        int goal = 0;

        while(true){
            System.out.println("enter in score you wish to find");
            goal = compare();
            if(goal < 0) {
                continue;
            }
            break;

        }

        int binary[] = binarySort(goal, scores);

        if(binary[0] == 1){
            System.out.println("found "+goal+" in "+binary[1]+" iterations");
        }else{
            System.out.println("did not find "+goal);
        }

        int[] random = randomize(scores);

        int target = compare();

        linear(random, target);






    }

    public static void linear(int[] scores, int target){
        for(int i = 0; i < scores.length; i++){
            if(scores[i] == target){
                System.out.println("Found in "+i+" moves");
                return;
            }
        }
        System.out.println("Not Found");
    }

    public static int[] randomize(int[] scores){
        for(int i = 0; i < scores.length/2; i++){
            int index = (int) Math.random()*scores.length;
            int bef = scores[index];
            scores[index] = scores[scores.length-index];
            scores[scores.length-index] = bef;
        }
        return scores;
    }

    public static int[] binarySort(int goal, int[] scores){
        int iterations = 1;
        int mid = scores.length/2;

        int max = scores.length-1;
        int min = 0;

        int midBef = -1;

        if(scores[mid] > scores[max]){
            int go[] = {0, iterations};
            return go;
        }
        if(scores[mid] < scores[min]){
            int go[] = {0, iterations};
            return go;
        }

        while(mid != goal) {

            if(scores[mid] == goal){
                int go[] = {1, iterations};
                return go;
            }

            if(scores[mid] < goal){
                mid = min;
            }
            if(scores[mid] > goal){
                max = min;
            }

            //issue is how to get it to agree it's wrong
            midBef = mid;
            mid = (max+min)/2;

            if(midBef == mid){
                int go[] = {0, iterations};
                return go;
            }

            System.out.println("mid: "+mid);
            System.out.println("midBef: "+midBef);

            iterations++;

        }
        return scores;
    }

    public static int compare(){

        Scanner myScanner = new Scanner(System.in);

        boolean check = myScanner.hasNextInt();
        while(!check){
            String garbage = myScanner.nextLine();
            System.out.println("Enter an integer");
            check = myScanner.hasNextInt();
        }
        int intermediate = myScanner.nextInt();

        if(intermediate > 100 || intermediate < 0){
            System.out.println("score needs to be within range 0 - 100");
            return -1;
        }


        return intermediate;
    }

}
