//Matthew J Gunton
//9/6/2018
//CSE02 - 311

//my program will do the following:
//given the number of seconds a trip took and the number of revolutions of the bicycle wheel for 2 trips, 
//it will display:
  //the total time of each trip in minutes
  //the total revolutions of the wheels for each trip
  //the total distance of each trip in miles
  //the total distance of the combined trips


public class Cyclometer{
  
  //this is the classic main method inside of each and every java program
  public static void main(String args[]){
    
     //input data:
    int secsTrip1 = 480; //seconds elapsed for trip 1
    int secsTrip2 = 3220; //seconds elapsed for trip 2
    int countsTrips1 = 1561; //number of revolutions of the bicycle wheels for trip 1
    int countsTrips2 = 9037; //number of revolutions of the biccyle wheels for trip 2
    
    //constants we use for calculations:
    double wheelDiameter = 27.0, //when converted to circumference, helps us find distance in junciton with the countsTrips
           PI = 3.14159, //helps us convert wheelDiamter into circumference, which we use to find distance of trip
           feetPerMile = 5280, //helps us convert our units from feet to mile
           inchesPerFoot = 12, //helps us convert our units from inches to feet
           secondsPerMinute = 60; //helps us convert our time from seconds to minutes
    double distanceTrip1, distanceTrip2, totalDistance;
    
    //output trip session:
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrips1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrips2+" counts.");
    
    //calculations for the total distance for each trip
    
      //to find distance, we need to know the circumference of the wheel(pi*D), and the number of revolutions of the wheels,
        //given by countsTrip
      //once we know the circumfernce of the wheel, we can multiply that by how many times we traveled the 
        //circumference(countsTrip) to get distance
      //finally, we have to convert our distance from inches to miles using (inchesPerFeet & feetPerMile)
      distanceTrip1 = (countsTrips1*wheelDiameter*PI)/(inchesPerFoot)/(feetPerMile);
      distanceTrip2 = (countsTrips2*wheelDiameter*PI)/(inchesPerFoot)/(feetPerMile);

      //to find total distance, add up distances of trip 1 and 2
      totalDistance = distanceTrip1 + distanceTrip2;
    
    //output the respective distances of each trip and the total distance
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("Total distance traveled was "+totalDistance+" miles");
    
  }//end of my main method
  
}//end of public class cyclometer
