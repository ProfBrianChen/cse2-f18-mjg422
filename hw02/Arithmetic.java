//Matthew J Gunton 09/6/2018 CSE02-311

//this program takes the number of an item and the cost of that item, then calculates the total cost for the transaction
//including pennsylvania state sales tax

//still need to make the numbers look like receipt numbers

public class Arithmetic{
  public static void main(String args[]){
    //items we're going to purchase
      //number of pants
      int numPants = 3;
      //cost of pants
      double pantsPrice = 34.98;
    
      //number of sweatshirts
      int numShirts = 2;
      //cost of shirts
      double shirtPrice = 24.99;
    
      //number of belts
      int numBelts = 1;
      //cost of belt
      double beltPrice = 33.99;
    
    //tax rate
    double paSalesTax = 0.06;
      
     //declaring variables we'll use to find the total cost
      double costOfPants,
             costOfShirt,
             costOfBelt,
             pantTax,
             shirtTax,
             beltTax,
             preTaxTotal,
             totalSalesTax,
             totalCost;
      
     
    //calculating total cost by items
      costOfPants = (double) numPants*pantsPrice;
      costOfShirt = (double) numShirts*shirtPrice;
      costOfBelt = (double) numBelts*beltPrice;
    
    //calculating and displaying sales tax of each item
      pantTax = 100*costOfPants*paSalesTax;
        //how we make sure the total always has only 2 digits at the end
          //imperfect because the method does not account for any rounding up
        int holder = (int) pantTax;
        pantTax = (double) holder/100;
      System.out.println("Total cost of pants: $"+costOfPants+". Sales tax on pants: $"+pantTax);
    
      shirtTax = 100*costOfShirt*paSalesTax;
        holder = (int) shirtTax;
        shirtTax = (double) holder/100;
      System.out.println("Total cost of shirts: $"+costOfShirt+". Sales tax on shirts: $"+shirtTax);
    
      beltTax = 100*costOfBelt*paSalesTax;
        holder = (int) beltTax;
        beltTax = (double) holder/100;
      System.out.println("Total cost of belts: $"+costOfBelt+". Sales tax on belts: $"+beltTax);
    
    //pretax cost
      preTaxTotal = 100*(costOfBelt+costOfShirt+costOfPants);
        holder = (int) preTaxTotal;
        preTaxTotal = (double) holder/100;
      System.out.println("Pre-Tax Total: $"+preTaxTotal);
    
    //total sales tax
      totalSalesTax = 100*(pantTax+shirtTax+beltTax);
        holder = (int) totalSalesTax;
        totalSalesTax = (double) holder/100;
      System.out.println("Total Sales Tax: $"+totalSalesTax);

    
    //total including tax
      totalCost = 100*(preTaxTotal+totalSalesTax);
        holder = (int) totalCost;
        totalCost = (double) holder/100;
      System.out.println("Total Cost: $"+totalCost);
    
  }
}