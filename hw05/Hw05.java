//finished hw05

//Matthew J Gunton CSE2-311
//program to:
//1 randomly generate poker hands (5 cards)
//say if the cards fit one of the possible hands

import java.util.Scanner;

public class Hw05{
    public static void main (String args[]){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("how many hands do you want drawn?");

        //makes sure the user's input is valid
        boolean isNumHandsInt = myScanner.hasNextInt();
        while(isNumHandsInt == false){
            String garbageCollector = myScanner.nextLine();
            System.out.println("enter in hand number in integer form");
            isNumHandsInt = myScanner.hasNextInt();
        }
        double numHands = myScanner.nextInt();

        //keeps track of how many target hands are randomly picked
        double onePair = 0,
                twoPair = 0,
                threeOfAKind = 0,
                fourOfAKind = 0;

        //draws the number of hands
        int counter = 0;
        while(counter < numHands){

            //fails to account for same card, different suit
            //2 of spades is two of a kind with 2 of diamonds
            // -13 for each possible one (great, more calculations!)
            //or just use the % and see if they go into each other perfectly

            //make sure none of the cards are the same number
            boolean uniqueCards = false;
            int cardOne = 0;
            int cardTwo = 0;
            int cardThree =0;
            int cardFour = 0;
            int cardFive = 0;

            while(uniqueCards == false){

                cardOne = (int)(Math.random()*52+1);
                cardTwo = (int)(Math.random()*52+1);
                cardThree = (int)(Math.random()*52+1);
                cardFour = (int)(Math.random()*52+1);
                cardFive = (int)(Math.random()*52+1);
                if( cardOne == cardTwo ||
                        cardOne == cardThree ||
                        cardOne == cardFour ||
                        cardOne == cardFive ||
                        cardTwo == cardThree ||
                        cardTwo == cardFour ||
                        cardTwo == cardFive ||
                        cardThree == cardFour ||
                        cardThree == cardFive||
                        cardFour == cardFive ){

                    System.out.println("hand drawn wasn't unique\ndrawing again");
                }else{
                    uniqueCards = true;
                }
            }

            System.out.println(cardOne+" "+cardTwo+" "+cardThree+" "+cardFour+" "+cardFive);

//       int matches = 0;

//       //I need to place the cards in order for the bottom to recognize them correctly
//       //put this in a while loop
//       boolean sorted = false;
//       while(sorted == false){
//         if(cardFive > cardFour){
//           int holder = cardFive;
//           cardFive = cardFour;
//           cardFour = holder;
//         }
//         if(cardFour > cardThree){
//           int holder = cardFour;
//           cardFour = cardThree;
//           cardThree = holder;
//         }
//         if(cardThree > cardTwo){
//           int holder = cardThree;
//           cardThree = cardTwo;
//           cardTwo = holder;
//         }
//         if(cardTwo > cardOne){
//           int holder = cardTwo;
//           cardTwo = cardOne;
//           cardOne = holder;
//         }
//         if( cardOne>cardTwo && cardTwo > cardThree && cardThree > cardFour && cardFour > cardFive){
//           sorted = true;
//         }
//       }

//       System.out.println("Sorted: "+cardOne+" "+cardTwo+" "+cardThree+" "+cardFour+" "+cardFive);

//       //one of a kind statements completed
//       if(cardOne % cardTwo  == 0 ||
//          cardOne % cardThree  == 0 ||
//          cardOne % cardFour  == 0 ||
//          cardOne % cardFive  == 0 ||
//          cardTwo % cardThree  == 0 ||
//          cardTwo % cardFour  == 0 ||
//          cardTwo % cardFive  == 0 ||
//          cardThree % cardFour  == 0 ||
//          cardThree % cardFive == 0 ||
//          cardFour % cardFive == 0 ){
//         System.out.println("onePair");
//         onePair++;
//       }

//       if( ((cardOne % cardTwo==0) && (cardTwo % cardThree==0)) ||
//          ((cardOne % cardTwo==0) && (cardTwo % cardFour==0)) ||
//          ((cardOne % cardTwo==0) && (cardTwo % cardFive==0)) ||
//          ((cardTwo % cardThree==0) && (cardThree % cardFour==0)) ||
//          ((cardTwo % cardThree==0) && (cardThree % cardFive==0)) ||
//          ((cardThree % cardFour==0) && (cardFour % cardFive==0)) ){
//         threeOfAKind++;
//                 System.out.println("threeOfAKind");
//       }

//       if( ((cardOne % cardTwo==0) && (cardTwo % cardThree==0) && (cardThree % cardFour==0)) ||
//          ( (cardTwo % cardThree==0) && (cardThree % cardFour==0) && (cardFour % cardFive==0) ) ){
//         fourOfAKind++;
//                 System.out.println("fourOfAKind");
//       }

//       if( ((cardOne % cardTwo == 0) && (cardThree % cardFour == 0)) ||
//          ((cardOne % cardTwo == 0) && (cardFour % cardFive == 0)) ||
//          ((cardOne % cardTwo == 0) && (cardThree % cardFive == 0)) ||
//          ((cardOne % cardThree == 0) && (cardTwo % cardFour == 0)) ||
//          ((cardOne % cardThree == 0) && (cardTwo % cardFive == 0)) ||
//          ((cardOne % cardThree == 0) && (cardFour % cardFive == 0)) ||
//          ((cardOne % cardFour == 0) && (cardTwo % cardThree == 0)) ||
//          ((cardOne % cardFour == 0) && (cardTwo % cardFive == 0)) ||
//          ((cardOne % cardFour == 0) && (cardThree % cardFive == 0)) ||
//          ((cardOne % cardFive == 0) && (cardTwo % cardThree == 0)) ||
//          ((cardOne % cardFive == 0) && (cardTwo % cardFour == 0)) ||
//          ((cardOne % cardFive == 0) && (cardThree % cardFive == 0)) ||
//          ((cardTwo % cardThree == 0) && (cardFour % cardFive == 0)) ||
//          ((cardTwo % cardFour == 0) && (cardThree % cardFive == 0)) ||
//          ((cardTwo % cardFive == 0) && (cardThree % cardFour == 0)) ){
//         twoPair++;
//                 System.out.println("twoPair");
//       }

            cardOne = cardOne % 13;
            cardTwo = cardTwo % 13;
            cardThree = cardThree % 13;
            cardFour = cardFour % 13;
            cardFive = cardFive % 13;

            //       //one of a kind statements completed
            if(cardOne == cardTwo ||
                    cardOne == cardThree ||
                    cardOne == cardFour ||
                    cardOne == cardFive ||
                    cardTwo == cardThree ||
                    cardTwo == cardFour ||
                    cardTwo == cardFive ||
                    cardThree == cardFour ||
                    cardThree == cardFive||
                    cardFour == cardFive){
                System.out.println("onePair");
                onePair++;
            }

            if( ((cardOne == cardTwo) && (cardTwo == cardThree)) ||
                    ((cardOne == cardTwo) && (cardTwo == cardFour)) ||
                    ((cardOne == cardTwo) && (cardTwo == cardFive)) ||
                    ((cardTwo == cardThree) && (cardThree == cardFour)) ||
                    ((cardTwo == cardThree) && (cardThree == cardFive)) ||
                    ((cardThree == cardFour) && (cardFour == cardFive)) ){
                threeOfAKind++;
                System.out.println("threeOfAKind");
            }

            if( ((cardOne == cardTwo) && (cardTwo == cardThree) && (cardThree == cardFour)) ||
                    ( (cardTwo == cardThree) && (cardThree == cardFour) && (cardFour == cardFive) ) ){
                fourOfAKind++;
                System.out.println("fourOfAKind");
            }

            if( ((cardOne == cardTwo) && (cardThree == cardFour)) ||
                    ((cardOne == cardTwo) && (cardFour == cardFive)) ||
                    ((cardOne == cardTwo) && (cardThree == cardFive)) ||
                    ((cardOne == cardThree) && (cardTwo == cardFour)) ||
                    ((cardOne == cardThree) && (cardTwo == cardFive)) ||
                    ((cardOne == cardThree) && (cardFour == cardFive)) ||
                    ((cardOne == cardFour) && (cardTwo == cardThree)) ||
                    ((cardOne == cardFour) && (cardTwo == cardFive)) ||
                    ((cardOne == cardFour) && (cardThree == cardFive)) ||
                    ((cardOne == cardFive) && (cardTwo == cardThree)) ||
                    ((cardOne == cardFive) && (cardTwo == cardFour)) ||
                    ((cardOne == cardFive) && (cardThree == cardFive)) ||
                    ((cardTwo == cardThree) && (cardFour == cardFive)) ||
                    ((cardTwo == cardFour) && (cardThree == cardFive)) ||
                    ((cardTwo == cardFive) && (cardThree == cardFour)) ){
                twoPair++;
                System.out.println("twoPair");
            }

            //iterate:
            counter++;

        }

        double finalFour = fourOfAKind / numHands;

        double finalThree = threeOfAKind / numHands;

        double finalTwo = twoPair / numHands;

        double finalOne = onePair / numHands;


        System.out.printf("\nThe probability of Four-of-a-kind: %2.3f",finalFour);
        System.out.printf("\nThe probability of Three-of-a-kind: %2.3f",finalThree);
        System.out.printf("\nThe probability of Two pair: %2.3f",finalTwo);
        System.out.printf("\nThe probability of One pair: %2.3f",finalOne);

    }
}
