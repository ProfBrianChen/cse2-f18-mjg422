////CSE 002 Matthew J Gunton

public class WelcomeClass{
  
  public static void main(String args[]){
    //these system commands send the string inside them to the console, in this case they are displaying the pattern I'm designing
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--J--G--4--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");

  }
}